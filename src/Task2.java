import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

public class Task2 {

    public static void main(String[] args) throws FileNotFoundException {
        FileOutputStream out = new FileOutputStream("out.txt");
        PrintStream w = new PrintStream(out);

        Thread one = new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                w.println("qwertyasdfgzxcvb");
                try {
                    Thread.sleep(20);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        Thread two = new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                w.println("hjhljtjrfjrf");
                try {
                    Thread.sleep(20);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        Thread three = new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                w.println("kdfgkjsdjfkgjfgdf");
                try {
                    Thread.sleep(20);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        one.start();
        two.start();
        three.start();
    }
}
