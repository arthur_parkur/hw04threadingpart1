
public class Task1 {
    private final Object monitor = new Object();
    private volatile char symbol = 'A';
    private final int count = 5;

    public static void main(String[] args) {
        Task1 m = new Task1();
        Thread one = new Thread(() -> {
            m.printA();
        });
        Thread two = new Thread(() -> {
            m.printB();
        });
        Thread three = new Thread(() -> {
            m.printC();
        });

        one.start();
        two.start();
        three.start();
    }

    public void printA() {
        synchronized (monitor) {
            try {
                for (int i = 0; i < count; i++) {
                    while (symbol != 'A') {
                        monitor.wait();
                    }
                    System.out.print("A");
                    symbol = 'B';
                    monitor.notifyAll();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void printB() {
        synchronized (monitor) {
            try {
                for (int i = 0; i < count; i++) {
                    while (symbol != 'B') {
                        monitor.wait();
                    }
                    System.out.print("B");
                    symbol = 'C';
                    monitor.notifyAll();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void printC() {
        synchronized (monitor) {
            try {
                for (int i = 0; i < count; i++) {
                    while (symbol != 'C') {
                        monitor.wait();
                    }
                    System.out.println("C");
                    symbol = 'A';
                    monitor.notifyAll();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
