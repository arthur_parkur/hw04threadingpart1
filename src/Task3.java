public class Task3 {

    public static void main(String[] args) {
        MFU mfu = new MFU();
        mfu.print(9);
    }
}

class MFU {
    private static final Object printM = new Object();
    private static final Object scanM = new Object();

    public void print(int pageCount) {
        Printer pnt = new Printer(pageCount);
        pnt.start();
    }

    public void scan(int pageCount) {
        Scanner scn = new Scanner(pageCount);
        scn.start();
    }

    private class Printer extends Thread {

        private int pageCount;
        public Printer(int pageCount) {
            this.pageCount = pageCount;
        }

        @Override
        public void run() {
            synchronized (printM) {
                for (int i = 1; i <= pageCount; i++) {
                    System.out.println("Печать страницы: " + i);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println("Печать завершена");
            }
        }
    }

    private class Scanner extends Thread {
        private int pageCount;
        public Scanner(int pageCount) {
            this.pageCount = pageCount;
        }

        @Override
        public void run() {
            synchronized (scanM) {
                for (int i = 1; i <= pageCount; i++) {
                    System.out.println("Сканирование страницы: " + i);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println("Сканирование завершена");
            }
        }
    }

}